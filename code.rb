# frozen_string_literal: true

require 'gitlab'

api_url = ENV['CI_API_V4_URL']
project_id = ENV['CI_PROJECT_ID']
pipeline_id = ENV['CI_PIPELINE_ID']
token = ENV['CI_JOB_TOKEN']

gitlab_client = ::Gitlab.client(endpoint: api_url, private_token: token)

pipeline_jobs = gitlab_client.pipeline_jobs(project_id, pipeline_id)

puts "Jobs in the pipeline are"
pipeline_jobs.each do |job|
  puts job.name
end
